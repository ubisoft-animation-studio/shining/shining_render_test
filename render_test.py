import argparse
import os
import shutil
import json
import sys
import subprocess
import platform
import xml.etree.ElementTree as ET

BASE_SCRIPT_PATH = os.path.dirname(os.path.abspath( __file__ ))
RENDER_TEST_DIR = BASE_SCRIPT_PATH
if not os.path.exists(RENDER_TEST_DIR):
    print "Directory \"" + RENDER_TEST_DIR + "\" doesn't exist. Exit the program !"
    exit(-1)

sys.path.append(RENDER_TEST_DIR)
from test_utils import *

DESCRIPTIONS_DIR = os.path.join(RENDER_TEST_DIR, "descriptions")
TOOLS_DIR = os.path.join(RENDER_TEST_DIR, "tools")
REPORT_TEMPLATE = os.path.join(TOOLS_DIR, "test_report.tpl")
REPORT_JS = os.path.join(TOOLS_DIR, "test_report_script.js")
OIIO_WIN = os.path.join(TOOLS_DIR, "oiiotool_win", "oiiotool.exe") if platform.system() == "Windows" else os.path.join(TOOLS_DIR, "oiiotool_lin", "oiiotool")
file_exists_or_quit(OIIO_WIN)

REFERENCES_DIR = "ref"
TEST_RESULTS_FILE = "test_results.json"
TEST_DURATIONS_FILE = "durations.json"
REPORT_FILE = "test_report.html"

ERROR_PREFIX = "_error_"
COMPARISON_THRESHOLD = "0.0005"

def image_filename_format(test_name, layer, _pass, frame, ext):
    return "{:s}_{:s}_{:s}_lnh.{:04d}.{:s}".format(test_name, layer, _pass, frame, ext)

def find_xml_tag_with_id(root_tag, target_tag_name, id):
    for t in root_tag.iter(target_tag_name):
        if t.get("id") == id:
            return t
    return None

class Comparator:
    def to_png(self, image_file):
        return os.path.join(os.path.dirname(image_file), os.path.splitext(image_file)[0] + ".png")

    def generate_png(self, image_file, type = ""):
        if not os.path.isfile(image_file):
            return

        cmd = [self.oiio]
        cmd.append(image_file)
        cmd.extend(["--iscolorspace", "sRGB"])
        if type == "error":
            cmd.extend(["--ch", "R,G,B"])
        cmd.extend(["--cpow", "0.4545"])

        png_file = self.to_png(image_file)
        cmd.extend(["-o", png_file])

        try:
            subprocess.check_call(cmd)
        except OSError:
            print_error("Cannot generate PNG: " + png_file)

    def delete_image(self, image_file):
        if not os.path.isfile(image_file):
            return

        os.remove(image_file)
        # delete an hypothetically previously generated png
        png_file = os.path.join(os.path.dirname(image_file), os.path.splitext(image_file)[0] + ".png")
        if os.path.isfile(png_file):
            os.remove(png_file)


    def call_oiio(self, ref_path, test_path, image_file):
        ### oiio
        cmd = [self.oiio]

        ### inputs
        ref_image = os.path.join(ref_path, image_file)
        if not os.path.isfile(ref_image):
            return False, "Unable to find " + ref_image + " - ERROR"

        test_image = os.path.join(test_path, image_file)
        if not os.path.isfile(test_image):
            return False, "Unable to find " + test_image + " - ERROR"

        cmd.extend([ref_image, test_image])

        cmd.extend(["--fail", COMPARISON_THRESHOLD, "--diff", "--sub", "--abs"])

        diff_image = os.path.join(test_path, ERROR_PREFIX + image_file)
        cmd.extend(["-o", diff_image])

        success = True
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        output,error = p.communicate()
        if output.find("FAILURE") != -1:
            success = False

            # Generate test and error png image for the report
            self.generate_png(test_image)
            self.generate_png(diff_image, "error")
        else:
            # To save disk space, we delete diff image if the test pass
            self.delete_image(diff_image)
            # We also delete test image because it is the same as the reference image, and it will be regenerated when the test will be relaunched
            self.delete_image(test_image)

        # Always generate ref png image for the report
        self.generate_png(ref_image)

        return success, output

    def compare(self, category, group, test, frame_range, settings):
        ref_path = os.path.join(self.render_ref_path, category, group)
        test_path = os.path.join(self.render_test_path, category, group)

        comparison_succeed = True

        passes = settings["parameters"]["pass"].split(",")
        layer = settings["parameters"]["layer"]

        test_res = open(self.test_results_path, "r")
        results_json = json.load(test_res)
        test_res.close()
        cat_json = get_or_create_from_key(results_json, category)
        group_json = get_or_create_from_key(cat_json, group)
        test_json = get_or_create_from_key(group_json, test)
        test_json["settings"] = settings
        test_json["passes"] = {}

        for p in passes:
            test_json["passes"][p] = []
            pass_json = test_json["passes"][p]
            for f in xrange(frame_range.istart, frame_range.iend, frame_range.step):
                image_file = image_filename_format(test, layer, p, f, "exr")
                success, oiio_msg = self.call_oiio(ref_path, test_path, image_file)

                frame_log = {}
                frame_log["image"] = image_file
                frame_log["frame"] = f
                frame_log["error"] = int(not success)
                splited_log = oiio_msg.replace("\r", "").split("\n")[1:] # remove some carriage return and the first line (which is not interesting)
                frame_log["log"] = ""
                for s in splited_log:
                    frame_log["log"] += s + "\n" 

                pass_json.append(frame_log)

                if success:
                    print_info("   |-> " + image_file + " > OK")
                else:
                    if oiio_msg.find("ERROR"):
                        print_error("   |-> " + image_file + " > " + oiio_msg)
                    else:
                        print_info("   |-> " + image_file + " > FAILURE")
                    comparison_succeed = False

        test_res = open(self.test_results_path, "w")
        test_res.write(json.dumps(results_json, indent=2))
        test_res.close()

        self.write_report()

        return comparison_succeed

    def write_report(self):
        results_file = open(self.test_results_path)
        results = json.load(results_file)
        results_file.close()

        ref_durations_json = load_json_file(os.path.join(self.render_ref_path, TEST_DURATIONS_FILE))
        test_durations_json = load_json_file(os.path.join(self.render_test_path, TEST_DURATIONS_FILE))

        report_tree = ET.parse(REPORT_TEMPLATE)
        test_count = 0
        error_count = 0

        overview_ol = find_xml_tag_with_id(report_tree, "ol", "test_overview")
        detailled_table = find_xml_tag_with_id(report_tree, "table", "test_detailled")

        cat_keys = results.keys()
        cat_keys.sort()
        for cat in cat_keys:
            group_list = results[cat]
            cat_li = ET.fromstring("<li><a href=\"#{0}\">{0}</a></li>\n".format(cat))
            overview_ol.append(cat_li)
            cat_ol = ET.fromstring("<ol class=\"tests_group\"></ol>\n")
            cat_li.append(cat_ol)

            detailled_table.append(ET.fromstring("<tr id=\"{0}\" class=\"category_tr\"><td>{0}</td><td></td><td></td><td></td><td></td><td></td></tr>".format(cat)))

            group_keys = group_list.keys()
            group_keys.sort()
            for group in group_keys:
                test_list = group_list[group]
                cat_group = merge_keys(cat, group)
                group_li = ET.fromstring("<li><a href=\"#{}\">{}</a></li>\n".format(cat_group, group))
                cat_ol.append(group_li)
                group_p = ET.fromstring("<p class=\"tests_test\"></p>\n")
                group_li.append(group_p)

                detailled_table.append(ET.fromstring("<tr id=\"{0}\" class=\"group_tr\"><td>{1}</td><td></td><td></td><td></td><td></td><td></td></tr>".format(cat_group, group)))

                test_keys = test_list.keys()
                test_keys.sort()
                for test in test_keys:
                    test_info = test_list[test]
                    test_count += 1

                    cat_group_test = merge_keys(cat_group, test)

                    test_a = ET.fromstring("<a href=\"#{0}\" class=\"\">{1}</a>\n".format(cat_group_test, test))
                    group_p.append(test_a)

                    test_tr = ET.fromstring("<tr id=\"{0}\" class=\"\"><td>{1}</td><td></td><td></td><td></td><td></td><td></td></tr>".format(cat_group_test, test))
                    detailled_table.append(test_tr)

                    # get test scope durations
                    ref_durations = ref_durations_json.get(cat_group_test) 
                    test_durations = test_durations_json.get(cat_group_test)

                    test_failed = False

                    passes = test_info["passes"]
                    pass_keys = passes.keys()
                    pass_keys.sort()
                    for _pass in pass_keys:
                        frames = passes[_pass]

                        pass_tr = ET.fromstring("<tr id=\"{0}_{2}\" class=\"\"><td>{1}_{2}</td><td></td><td></td><td></td><td></td><td></td></tr>".format(cat_group_test, test, _pass))
                        detailled_table.append(pass_tr)

                        pass_failed = False
                        count_token = 0
                        for f in frames:
                            failed = f["error"] == 1

                            frame_line = "<tr id=\"{0:s}_{1:s}_{2:04d}\" class=\"{3:s}\">".format(cat_group_test, _pass, f["frame"], "frame_tr in_error" if failed else "frame_tr")
                            frame_line += "<td><a href=\"{0:s}\" class=\"frame_name\" target=\"_blank\">{1:s}_{2:s}_{3:04d}</a></td>".format("#", test, _pass, f["frame"])

                            png_image = self.to_png(f["image"])
                            # note: for the report to work, it must be placed in a folder directly next to the reference image folder
                            ref_image_path = os.path.join(self.render_ref_base_img_path, cat, group, png_image)
                            frame_line += "<td class=\"image_td\"><a href=\"{0:s}\" target=\"_blank\"><img src=\"{0:s}\" width=\"160\"/></a></td>".format(ref_image_path)
                            if failed:
                                test_image_path = os.path.join(".", cat, group, png_image)
                                frame_line += "<td class=\"image_td\"><a href=\"{0:s}\" target=\"_blank\"><img src=\"{0:s}\" width=\"160\"/></a></td>".format(test_image_path)
                                error_image_path = os.path.join(".", cat, group, ERROR_PREFIX + png_image)
                                frame_line += "<td class=\"image_td\"><a href=\"{0:s}\" target=\"_blank\"><img src=\"{0:s}\" width=\"160\"/></a></td>".format(error_image_path)
                            else:
                                frame_line += "<td class=\"image_td\">-</td>"
                                frame_line += "<td class=\"image_td\">-</td>"
                            frame_line += "<td>{0:s}</td>".format(f["log"].replace("\n", "<br/>"))

                            ref_dur = 0
                            if ref_durations is not None and count_token < len(ref_durations):
                                ref_dur = ref_durations[count_token]

                            test_dur = 0
                            if test_durations is not None and count_token < len(test_durations):
                                test_dur = test_durations[count_token]

                            frame_line += "<td class=\"durations\"><table><tr><td>ref</td><td class=\"time\">{}</td></tr><tr><td>test</td><td class=\"time\">{}</td></tr></table></td>".format(pretty_str_time(ref_dur), pretty_str_time(test_dur))
                            frame_line += "</tr>"
                            detailled_table.append(ET.fromstring(frame_line))

                            test_failed = test_failed or failed
                            pass_failed = pass_failed or failed
                            count_token += 1

                        pass_tr.set("class", "pass_tr in_error" if pass_failed else "pass_tr")

                    test_a.set("class", "test_link in_error" if test_failed else "test_link")
                    test_tr.set("class", "test_tr in_error" if test_failed else "test_tr")
                    if test_failed:
                        error_count += 1


        error_count_span = find_xml_tag_with_id(report_tree, "span", "failed_test_counter")
        error_count_span.text = str(error_count)
        test_count_span = find_xml_tag_with_id(report_tree, "span", "total_test_counter")
        test_count_span.text = str(test_count)

        report_tree.write(os.path.join(self.render_test_path, REPORT_FILE))

    def write_xml(self, xml_path, durations_dict):
        safe_mkdirs(os.path.dirname(xml_path))
        with open(self.test_results_path, "r") as f:
            j = json.load(f)

            test_count = 0
            error_count = 0
            results_str = ""
            for cat, group_list in j.items():
                for group, test_list in group_list.items():
                    classname = "shining.render." + cat + "." + group
                    for test, test_info in test_list.items():
                        test_count +=1

                        # gather errors
                        error_str = ""
                        for _pass, frame_list in test_info["passes"].items():
                            for frame in frame_list:
                                if frame["error"] == 1:
                                    error_str += "Error on pass {} - frame {} :&#10;{}".format(_pass, frame["frame"], frame["log"].replace("\n", "&#10;"))

                        total_durations = durations_dict[test_key(cat, group, test)]

                        # write the test case
                        results_str += "<testcase classname=\"{}\" name=\"{}\" time=\"{}\"".format(classname, test, total_durations)
                        if error_str == "":
                            results_str += "/>"
                        else:
                            error_count += 1
                            results_str += "><failure type=\"error\" message=\"{}\"></failure></testcase>".format(error_str)

            xml_str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            xml_str += "<testsuite name=\"render_check\" tests=\"{}\" errors=\"0\" failures=\"{}\" skip=\"0\">".format(test_count, error_count)
            xml_str += results_str
            xml_str += "</testsuite>"

            with open(xml_path, "w") as xml:
                xml.write(xml_str)


    def __init__(self, render_ref_path, render_test_path, use_absolute_path_to_ref_for_html_report):
        self.oiio = OIIO_WIN
        self.render_ref_path = os.path.abspath(render_ref_path)
        if use_absolute_path_to_ref_for_html_report:
            self.render_ref_base_img_path = self.render_ref_path
        else:
            self.render_ref_base_img_path = os.path.join("..", os.path.basename(self.render_ref_path))
        self.render_test_path = os.path.abspath(render_test_path)

        self.test_results_path = os.path.join(self.render_test_path, TEST_RESULTS_FILE)
        shutil.copyfile(REPORT_JS, os.path.join(self.render_test_path, os.path.basename(REPORT_JS)))

        if not os.path.isfile(self.test_results_path):
            test_res = open(self.test_results_path, "w")
            print >> test_res, "{\n}"
            test_res.close()

class ShibInstance:

    def call_shib(self, input_file_base, render_file, settings, frame, shutter, shutter_offset):
        ### shib
        cmd = [self.shib]

        ### log string
        shib_log = ""

        ### arguments
        for key,value in settings["parameters"].items():
            if key in RENDER_SETTINGS:
                cmd.append(RENDER_SETTINGS[key])
            else:
                cmd.append("--" + key.replace("_", "-"))
            cmd.append(str(value))

        if not args.quiet:
            cmd.append("--progress")

        ### time
        formatted_time = str(frame)
        if shutter > 0.0:
            formatted_time = "{}:{}:{}".format(frame, frame + shutter, shutter_offset)
        cmd.append("--time")
        cmd.append(formatted_time)

        cmd.append("--report")
        cmd.append(render_file.replace(".exr", ".html").replace("_%p", ""))

        ### textures root
        cmd.append("--root")
        cmd.append("{},{}".format(PROXY_BASE_DIR, self.input_base))
        #cmd.append("--no-texturecheck")

        ### output
        cmd.append("-o")
        cmd.append(render_file)

        ### inputs
        cmd.append(input_file_base + ".bioc")
        cmd.append(input_file_base + ".json")

        shib_log += "\n=========== FRAME " + str(frame) + " ===========\n\n" 

        shib_log += ' '.join(cmd) + '\n\n'

        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        for line in iter(p.stdout.readline, ''):
            log_line = line.rstrip()
            shib_log += log_line + '\n'
            if not self.quiet and log_line.find("TR_PROGRESS") != -1:
                inline_print_info("   |-> " + log_line)
                sys.stdout.flush()
        output,error = p.communicate()
        if p.returncode != 0:
            print ""
            return False, shib_log

        return True, shib_log

    def render(self, category, group, test, frame_range, settings):
        with pushd(os.path.dirname(self.shib)):
            render_path = os.path.join(self.output_base, category, group)
            safe_mkdirs(render_path)

            layer = settings["parameters"]["layer"]

            input_files = os.path.join(self.dataset, category, group, test)

            log_file_path = os.path.join(render_path, test + ".log")
            error_log_file_path = os.path.join(render_path, test + "_error.log")

            # if the log_file exists, we suppossed the render was already done
            # pass the rendering if we don't have the force_rerender flag
            if os.path.exists(log_file_path) and not self.force_rerender:
                print_info("   |-> Already rendered. Passed.")
                return True

            log_file = None
            try:
                log_file = open(log_file_path, "w", 0)
            except IOError:
                print_warning("Can't create log file: " + log_file_path)

            # Open the durations log json file
            durations_json = load_json_file(self.durations_log_file)
            if durations_json is None:
                durations_json = {}
            duration_key = merge_keys(category, merge_keys(group, test))
            durations_json[duration_key] = []

            no_error = True
            for f in xrange(frame_range.istart, frame_range.iend, frame_range.step):
                start_time = get_current_time()

                render_file = os.path.join(render_path, image_filename_format(test, layer, "%p", f, "exr"))
                shib_succeed, shib_log = self.call_shib(input_files, render_file, settings, f, frame_range.shutter, frame_range.shutter_offset)
                no_error = no_error and shib_succeed
                duration = duration_from(start_time)

                if shib_succeed:
                    print_info("   |-> Image rendered: {:s} ({:f}s)".format(render_file, duration))
                else:
                    print_error("Something went wrong during the rendering !")
                    print shib_log
                    print

                durations_json[duration_key].append(duration_from(start_time))
                print >> log_file, shib_log

            log_file.close()

            if not no_error:
                shutil.move(log_file_path, error_log_file_path)
            else:
                if os.path.isfile(error_log_file_path):
                    os.remove(error_log_file_path)

            # Save durations log
            with open(self.durations_log_file, "w") as durations_file:
                durations_file.write(json.dumps(durations_json, indent=2))

            return no_error

    def __init__(self, shib_path, input_path, dataset_path, output_path, quiet, force_rerender = False):
        self.shib = os.path.abspath(shib_path)
        self.input_base = os.path.abspath(input_path)
        self.dataset = os.path.abspath(dataset_path)
        self.output_base = os.path.abspath(output_path)
        self.quiet = quiet
        self.force_rerender = force_rerender

        file_exists_or_quit(self.shib)
        dir_exists_or_quit(self.dataset)
        safe_mkdirs(output_path)

        # create correct void durations log file if it doesn't exist
        self.durations_log_file = os.path.join(self.output_base, TEST_DURATIONS_FILE)
        if not os.path.isfile(self.durations_log_file):
            with open(self.durations_log_file, "w") as duration_file:
                duration_file.write("{\n}")

if __name__ == "__main__" :
    parser = argparse.ArgumentParser(description="Render test images with two different shining versions and compare them.")
    parser.add_argument("-i", "--input_path", type=str, nargs="?", required=True, help="Path to the directory where all inputs data (bioc, json, textures, ...) are.")
    parser.add_argument("-o", "--output_path", type=str, nargs="?", required=True, help="Path to the directory where the output images are generated.")
    parser.add_argument("--shn_ref_prefix", type=str, nargs="?", required=False, help="Prefix path to used before shn_ref. Useful when declaring shn_ref in json config file instead of command line.")
    parser.add_argument("--shn_ref", type=str, nargs="?", required=False, help="Reference version of Shib exe or path to precomputed reference images if --no_render_ref if specified.")
    parser.add_argument("--shn_test", type=str, nargs="?", required=False, help="To test version of Shib exe. If not provided, only references images are rendered in directory specified with -o.")
    parser.add_argument("-d", "--dataset_tag", type=str, nargs="?", help="Dataset tag. If not set, the script will use the \"default\" folder in the input directory.")
    parser.add_argument("-t", "--output_test_tag", type=str, nargs="?", help="Tested output render tag. If not set, the test render images will be generated in \"YYYY-MM-DD\" folder \
        in the output directory. The syntax matches the one of strftime, so a tag like '%%m-%%d' would result in 'MM-DD' as folder name.")
    parser.add_argument("-f", "--filter", type=str, nargs="?", help="Filter specific tests, category or group to generate only these tests. Pattern : \"category:group:test\". \
        You don't have to specify each filter tier (e.g. you can write \"category:group\"). You can exclude a tier by using '~' (e.g. \"category:~group\" will select every tests in category, except those in the group group)")
    parser.add_argument("--xml", type=str, nargs="?", help="If a path is set, output a results xml with jenkins compatibility")
    parser.add_argument("--quiet", action="store_true", help="Disable rendering console prints and Shining progress")
    parser.add_argument("--no_render_ref", action="store_true", help="Disable rendering of reference images and use pre-rendered ones instead.")
    parser.add_argument("--config", type=str, nargs="?", help="json file containing configuration dataset_tag, filter and shn_ref. Command line arguments remain prefered if provided.")
    parser.add_argument("--use_absolute_path_to_ref_for_html_report", action="store_true", default=False, help="If specified, use absolute path to png images in the report.")
    args = parser.parse_args()

    if not args.shn_ref_prefix:
        args.shn_ref_prefix = ""
    
    if args.config:
        with open(args.config, 'r') as f:
            config_json = json.load(f)
            if 'dataset_tag' in config_json and not args.dataset_tag:
                args.dataset_tag = config_json['dataset_tag']
            if 'filter' in config_json and not args.filter:
                args.filter = config_json['filter']
            if 'shn_ref' in config_json and not args.shn_ref:
                args.shn_ref = config_json['shn_ref']

    ## shn_ref required
    if not args.shn_ref:
        print_error("shn_ref not specified. It should be specified either on the command line or in json config file.")
        exit(-1)

    args.shn_ref = os.path.join(args.shn_ref_prefix, args.shn_ref)

    ### Early exits
    dataset_tag = args.dataset_tag if args.dataset_tag is not None else DEFAULT_OUTPUT_TAG
    dataset_path = os.path.abspath(os.path.join(args.input_path, dataset_tag))

    tests_start_time = get_current_time()

    ### Setup all specific instances
    render_ref_and_compare = args.shn_test and not args.no_render_ref
    only_compare = args.shn_test and args.no_render_ref
    only_render_ref = not args.shn_test
    if render_ref_and_compare:
        ref_img_path = os.path.join(args.output_path, REFERENCES_DIR)
    elif only_compare:
        ref_img_path = args.shn_ref
    elif only_render_ref:
        ref_img_path = args.output_path # Write directly to output path if no comparison is asked

    if only_compare:
        dir_exists_or_quit(ref_img_path)
        file_exists_or_quit(os.path.join(ref_img_path, TEST_DURATIONS_FILE))

    test_tag = get_current_day(DATE_FORMAT if not args.output_test_tag else args.output_test_tag)

    if render_ref_and_compare:
        test_img_path = os.path.join(args.output_path, test_tag)
    elif only_compare:
        test_img_path = args.output_path

    if not args.no_render_ref:
        shib_ref = ShibInstance(args.shn_ref, args.input_path, dataset_path, ref_img_path, args.quiet)
    if args.shn_test:
        shib_test = ShibInstance(args.shn_test, args.input_path, dataset_path, test_img_path, args.quiet, True)
        comparator = Comparator(ref_img_path, test_img_path, args.use_absolute_path_to_ref_for_html_report)

    ### Get default bake settings
    default_settings = load_json_file(os.path.join(DESCRIPTIONS_DIR, DEFAULT_TEST_SETTINGS), "quit")

    ### Scanning test_set dir to list renders to do
    test_descs = scanTestDescriptionsFromJsonFiles(DESCRIPTIONS_DIR, default_settings, args.filter)
    test_count = len(test_descs)

    print_info("[ {} tests listed ]\n".format(test_count))

    ### Test execution durations, to store in output XML
    durations_dict = {}

    ### Iter on tests
    all_tests_succeed = True
    no_error = True
    count_token = 0
    failed_tests = []
    for desc in test_descs:
        count_token += 1
        category = desc["category"]
        group = desc["group"]
        test = desc["test"]
        try:
            print_info("-- [{:d}/{:d}] Start test {}".format(count_token, test_count, test_key(category, group, test)))

            settings = desc["settings"]
            fr = FrameRange(settings)
            print_info("  |-> " + fr.print_lite())

            start_time = get_current_time()

            if not args.no_render_ref:
                print_info("  |-> Reference images:")
                no_error_ref = shib_ref.render(category, group, test, fr, settings)
                no_error = no_error and no_error_ref

            if args.shn_test:
                print_info("  |-> Test images:")
                no_error_test = shib_test.render(category, group, test, fr, settings)
                no_error = no_error and no_error_test

                print_info("  |-> Compare images : ")
                current_test_succeed = comparator.compare(category, group, test, fr, settings)
                all_tests_succeed = all_tests_succeed and current_test_succeed
                if not current_test_succeed:
                    failed_tests.append(desc)
                print ""

                durations_dict[test_key(category, group, test)] = duration_from(start_time)

        except KeyError as e:
            print_error("Wrong format for test \"{}\" (Missing key: {:s})\n".format(test_key(category, group, test), str(e)))

    if args.shn_test and args.xml is not None:
        print_info("Write results XML")
        comparator.write_xml(args.xml, durations_dict)

    print_info("Tests total duration : " + pretty_str_time(duration_from(tests_start_time)))

    if all_tests_succeed and no_error:
        print_info("Finished with success !")
        sys.exit(0)
    else:
        print_info("Finished with some failures :(")
        print_info("{} / {} failed tests:".format(len(failed_tests), test_count))
        for desc in failed_tests:
            category = desc["category"]
            group = desc["group"]
            test = desc["test"]
            print_info("  -> {}".format(test_key(category, group, test)))
        sys.exit(1)

       
