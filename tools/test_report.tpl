<html>
<head>
	<title>Shining Test Results</title>
	<style type="text/css">
	body {
		width: 100%;
		background-color: #282828;
		color: #fff;
		font-family: sans-serif;
		padding: 0;
		margin : 0;
	}

	body a {
		color: #fff;
		text-decoration: none;
	}

	body a:hover {
		text-decoration: underline;
	}

	ol {
		list-style-type: disc;
		padding-left: 20px;
	}

	table {
		border-collapse: collapse;
		margin: 0;
		padding: 0;
	}

	tr {
		margin: 0;
		padding: 0;
	}

	td {
		margin: 0;
		padding: 5px;
	}
	
	body h1 {
		margin:0;
		padding: 5px;
		background: #181818
	}

	body h2 {
		margin: 10px 0px 0px 10px
	}

	#test_counters {
		margin:5px 0px 10px 10px;
		border:solid 1px;
		padding:5px;
		display: inline-block;
	}

	#test_overview {
		margin: 0px 0px 20px 15px;
		font-size: 12px;
	}

	#test_overview .tests_test {
		margin: 3px 0px 3px 20px;
		word-wrap: break-word;
	}

	#test_overview .tests_test a{
		display:inline-block;
		padding: 5px;
		margin: 1px;
		background: #383838
	}

	#test_detailled {
		width:98%;
		margin-top:10px;
		margin-left:15px;
	}

	#test_detailled .title {
		background: #181818;
		font-size: 25px;
	}

	#test_detailled .category_tr {
		font-size: 20px;
		font-weight: bold;
		background-color: #686868;
		width: 100%;
	}

	#test_detailled .group_tr {
		font-size: 15px;
		font-style: italic;
		background-color: #585858;
		width: 100%;
	}

	#test_detailled .test_tr {
		font-size: 15px;
		background-color: #484848;
	}

	#test_detailled .pass_tr	{
		font-size: 13px;
		background-color: #383838;
	}

	#test_detailled .frame_tr {
		font-size: 13px;
		font-style: italic;
	}

	#test_detailled .frame_tr .image_td {
		min-width: 160px;
		width: 160px;
	}

	#test_detailled  tr.frame_tr.in_error {
		background: #533;
		color: #fff;
	}

	td.durations table {
		font-size: 12px;
		font-style: italic;
	}

	td.time {
		font-size: 17px;
		font-weight: bold;
	}

	.in_error {
		color:#f55;
	}

	.clear {
	  clear: both;
	}

	#title span {
		float: right;
		font-size: 15px;
		margin-top: 7px;
	}
	</style>

	<script type="text/javascript" src="test_report_script.js">
		
	</script>
</head>
<body>
	<h1 id="title">Shining test results <span><input id="display_error_checkbox" type="checkbox" />display errors only</span></h1>

	<h2>Overview</h2>
	<p id="test_counters">Failed tests : <span id="failed_test_counter">0</span> / Total tests : <span id="total_test_counter">0</span></p>
	<ol id="test_overview">	
	</ol>

	<h2>Detailed tests results</h2>
	<table id="test_detailled">
		<tr class="title"><td>Test name</td><td>Reference</td><td>Test</td><td>Error</td><td>Log</td><td>Durations</td></tr>
	</table>
</body>

</html>